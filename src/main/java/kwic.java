import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class kwic{
    public ArrayList<String> ignored_words(ArrayList<String> input) {
        ArrayList<String> words = new ArrayList<String>();
        int t = 0;
        while(input.get(t)!="::"){
            words.add(input.get(t));
            t++;
        }
        return words;
        }
    public ArrayList<String> get_titles(ArrayList<String> input) {
            ArrayList<String> title = new ArrayList<String>();
            int t = 0;
            while(input.get(t)!="::"){
                t++;
            }
            t++;
            for(int i = t; i<input.size(); i++){
                title.add(input.get(i));
            }
            return title;
    }
    public ArrayList<String> get_keyword(ArrayList<String> input){
        ArrayList<String> ignore = ignored_words(input);
        ArrayList<String> title = get_titles(input);
        ArrayList<String> keyword = new ArrayList<String>();
        int t = 0;
        for(int i=0;i<title.size();i++){
            String single_title = title.get(i);
            for(String j : single_title.split(" ")){
                keyword.add(j);
            }
        }
        for(int i=0;i<ignore.size();i++){
            for(int j=0;j< keyword.size();j++){
                if(ignore.get(i).compareToIgnoreCase(keyword.get(j))==0){
                    keyword.remove(j);
                    j--;
                }
            }
        }

        HashSet<String> hs=new HashSet<>();
        hs.addAll(keyword);
        keyword.clear();
        keyword.addAll(hs);
        keyword.sort(String::compareToIgnoreCase);

        return keyword;

    }


}