import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
import java.lang.reflect.Array;

class kwicTest {

    @Test
    void ignored_words() {
        ArrayList<String> input = new ArrayList<>();
        input.add("is" );
        input.add("the" );
        input.add("of" );
        input.add("and" );
        input.add("as" );
        input.add("a" );
        input.add("but" );
        input.add("::" );
        input.add("Descent of Man" );
        input.add("The Ascent of Man" );
        input.add("The Old Man and The Sea" );
        input.add("A Portrait of The Artist As a Young Man" );
        input.add("A Man is a Man but Bubblesort IS A DOG" );
        ArrayList<String> expected= new ArrayList<>();
        expected.add("is");
        expected.add("the");
        expected.add("of" );
        expected.add("and" );
        expected.add("as" );
        expected.add("a" );
        expected.add("but" );
        assertEquals(new kwic().ignored_words(input), expected);
    }

    @Test
    void testTitles() {
        ArrayList<String> input = new ArrayList<>();
        input.add("is" );
        input.add("the" );
        input.add("of" );
        input.add("and" );
        input.add("as" );
        input.add("a" );
        input.add("but" );
        input.add("::" );
        input.add("Descent of Man" );
        input.add("The Ascent of Man" );
        input.add("The Old Man and The Sea" );
        input.add("A Portrait of The Artist As a Young Man" );
        input.add("A Man is a Man but Bubblesort IS A DOG" );
        ArrayList<String> expected= new ArrayList<>();
        expected.add("Descent of Man" );
        expected.add("The Ascent of Man" );
        expected.add("The Old Man and The Sea" );
        expected.add("A Portrait of The Artist As a Young Man" );
        expected.add("A Man is a Man but Bubblesort IS A DOG" );
        assertEquals(new kwic().get_titles(input), expected);
    }
    @Test
    void testKeywordSorted(){
        ArrayList<String> input = new ArrayList<>();
        input.add("is" );
        input.add("the" );
        input.add("of" );
        input.add("and" );
        input.add("as" );
        input.add("a" );
        input.add("but" );
        input.add("::" );
        input.add("Descent of Man" );
        input.add("The Ascent of Man" );
        input.add("The Old Man and The Sea" );
        input.add("A Portrait of The Artist As a Young Man" );
        input.add("A Man is a Man but Bubblesort IS A DOG" );
        ArrayList<String> expected= new ArrayList<>();
        expected.add("Artist");
        expected.add("Ascent");
        expected.add("Bubblesort");
        expected.add("Descent");
        expected.add("DOG");
        expected.add("Man");
        expected.add("Old");
        expected.add("Portrait");
        expected.add("Sea");
        expected.add("Young");
        assertEquals(new kwic().get_keyword(input), expected);

    }

}